create table player
    (
        id         serial primary key,
        ip_address    varchar(255) not null DEFAULT '0.0.0.0:0000',
        name       varchar(30) not null,
        count_wins integer default 0,
        count_lose integer default 0,
        points_scored  integer default 0
    );

create table game
    (
        id            serial primary key,
        date          date,
        first_player    integer not null, foreign key (first_player) references player (id),
        second_player    integer not null, foreign key (second_player) references player (id),
        fist_player_shots_count integer default 0,
        second_player_shots_count integer default 0,
        duration_game bigint

    );

create table shot
    (
        id          serial primary key,
        shot_time   timestamp,
        game_id     integer not null,foreign key (game_id) references game(id),
        shooter     integer not null, foreign key (shooter) references player (id),
        target integer not null, foreign key (target) references player (id)

    );
