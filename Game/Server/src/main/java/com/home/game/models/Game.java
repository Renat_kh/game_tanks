package com.home.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor

public class Game {
    private Integer id;
    private Timestamp gameDate;
    private Player firstPlayer;
    private Player secondPlayer;
    private Integer fistPlayerShotsCount;
    private Integer secondPlayerShotsCont;
    private Long durationGame;

}
