package com.home.game.services;

import com.home.game.models.*;
import com.home.game.repositories.*;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.util.Optional;

import static java.sql.Timestamp.*;
import static java.sql.Timestamp.valueOf;

public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;

    private GamesRepository gamesRepository;

    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Integer startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        // получили первого игрока
        Player first = checkIxExists(firstIp, firstPlayerNickname);
        // получили второго игрока
        Player second = checkIxExists(secondIp, secondPlayerNickname);
        // создаем игру
        Game game = Game.builder()
                .firstPlayer(first)
                .secondPlayer(second)
                .gameDate(new Timestamp(System.currentTimeMillis()))
                .fistPlayerShotsCount(0)
                .secondPlayerShotsCont(0)
                .durationGame(System.currentTimeMillis())
                .build();
        gamesRepository.save(game);
        return game.getId();
    }

    @Override
    public void shot(Integer id, String shooterNickname, String targetNickname) {
        // получаем информацию об игроках
        Player shooter = playersRepository.findByName(shooterNickname).get();
        Player target = playersRepository.findByName(targetNickname).get();
        // находим игру
        Game game = gamesRepository.findById(id);
        // создаем выстрел
        Shot shot = Shot.builder()
                .shooter(shooter)
                .target(target)
                .shotTime(new Timestamp(System.currentTimeMillis()))
                .game(game)
                .build();
        // увеличили очки у того, кто стрелял
        shooter.setPointsScored(shooter.getPointsScored() + 1);
        // если стрелял первый игрок
        if (game.getFirstPlayer().getId().equals(shooter.getId())) {
            // увеличиваем количество попаданий в этой игре
            game.setFistPlayerShotsCount(game.getFistPlayerShotsCount() + 1);
        }

        // если стрелял второй игрок
        if (game.getSecondPlayer().getId().equals(shooter.getId())) {
            game.setSecondPlayerShotsCont(game.getSecondPlayerShotsCont() + 1);
        }

        // обновляем данные по стреляющему
        playersRepository.update(shooter);
        // обновляем данные по игре
        gamesRepository.update(game);
        // сохраняем выстрел
        shotsRepository.save(shot);
    }


    // ищет игрока по никнейму, если такой игрок был - она меняет его IP
    // если игрока не было, она создает нового и сохраняет его
    private Player checkIxExists(String ipAddress, String name) {
        Player result;

        Optional<Player> playerOptional = playersRepository.findByName(name);
        // если игрока под таким именем нет
        if (!playerOptional.isPresent()) {
            // создаем игрока
            Player player = Player.builder()
                    .ipAddress(ipAddress)
                    .name(name)
                    .countWins(0)
                    .countLose(0)
                    .pointsScored(0)
                    .build();
            // сохраняем его в репозитории
            playersRepository.save(player);
            result = player;
        } else {
            Player player = playerOptional.get();
            player.setIpAddress(ipAddress);
            playersRepository.update(player);
            result = player;
        }

        return result;

    }

    @Override
    public void finishGame(Integer gameId, String winnerNickname, String loserNickname) {
        Game game = gamesRepository.findById(gameId);
        Player firstPlayer = playersRepository.findByName(winnerNickname).get();
        Player secondPlayer = playersRepository.findByName(loserNickname).get();


        if (firstPlayer.getName().equals(winnerNickname)) {
            // увеличиваем количество побед у первого игрока
            firstPlayer.setCountWins(firstPlayer.getCountWins() + 1);
            // увеличиваем количество поражений у второго игрока
            secondPlayer.setCountLose(secondPlayer.getCountLose() + 1);
        } else if  (firstPlayer.getName().equals(loserNickname)){
            // увеличиваем количество побед у второго игрока
            secondPlayer.setCountWins(secondPlayer.getCountWins() + 1);
            // увеличиваем количество поражений у первого игрока
            firstPlayer.setCountLose(firstPlayer.getCountLose() + 1);
        }
        // считаем разницу во времени начала и окончания игры в секундах
        Long endTime = System.currentTimeMillis();
        Long startTime = game.getDurationGame();
       game.setDurationGame((endTime - startTime) / 1000);
        //обновляем данные
        playersRepository.update(firstPlayer);
        playersRepository.update(secondPlayer);
        gamesRepository.update(game);

        System.out.println("Победитель: " + firstPlayer.getName() + "; Очки поподаний " + firstPlayer.getPointsScored()
                + "\n" +
                "Проигравший: " + secondPlayer.getName()  + "; Очки поподаний " + secondPlayer.getPointsScored());
    }
}