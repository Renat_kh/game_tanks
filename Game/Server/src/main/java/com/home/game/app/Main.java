package com.home.game.app;

import com.home.game.repositories.*;

import com.home.game.services.GameService;
import com.home.game.services.GameServiceImpl;

import com.home.game.sockets.GameServer;
import com.home.game.util.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;


public class Main {
    public static void main(String[] args) {

        HikariConfig hikariconfig = new HikariConfig();
        // создали DataSource - источник данных на основе конфига
        HikariDataSource dataSource = hikariconfig.dataSource();

        // создаем репозиторий, который использует этот источник данных
        GamesRepository gamesRepository = new GamesRepositoryImpl(dataSource);
        PlayersRepository playersRepository = new PlayersRepositoryImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryImpl(dataSource);

        // создали сервис, который использует созданные выше репозитории
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        // передали сервис
        GameServer gameServer = new GameServer(gameService);
        gameServer.start(7777);
    }
}
