package com.home.game.repositories;

public interface CrudRepository<R> {
    //Метод сохранения
    void save(R r);
}
