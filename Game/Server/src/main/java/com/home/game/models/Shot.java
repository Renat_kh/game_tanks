package com.home.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor

public class Shot {
    private Integer id;
    private Timestamp shotTime;
    private Game game;
    private Player shooter;
    private Player target;

}