package com.home.game.repositories;

import com.home.game.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

public class ShotsRepositoryImpl implements ShotsRepository {

    private static final String SQL_INSERT =
            "insert into shot (shot_time, game_id, shooter, target) values (?, ?, ?, ?);";

    private DataSource dataSource;

    public ShotsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setTimestamp(1, shot.getShotTime());
            statement.setInt(2, shot.getGame().getId());
            statement.setInt(3, shot.getShooter().getId());
            statement.setInt(4, shot.getTarget().getId());
            int update = statement.executeUpdate();

            if (update != 1) {
                throw new SQLException("Cannot save shot");
            }

            ResultSet keys = statement.getGeneratedKeys();
            if (keys.next()) {
                int generatedIdIn = keys.getInt("id");
                shot.setId(generatedIdIn);
            } else {
                throw new SQLException("Cannot return id");
            }
            keys.close();


        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
