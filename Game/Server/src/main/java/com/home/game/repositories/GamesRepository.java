package com.home.game.repositories;

import com.home.game.models.Game;

public interface GamesRepository extends CrudRepository<Game> {

    void update(Game game);
    Game findById(Integer id);


}

