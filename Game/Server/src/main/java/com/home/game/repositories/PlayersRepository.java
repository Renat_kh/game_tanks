package com.home.game.repositories;

import com.home.game.models.Player;

import java.util.Optional;

public interface PlayersRepository extends CrudRepository<Player> {

    void update(Player player);
    Optional<Player> findByName(String name);
    Player getByNickname(String nickname);

}

