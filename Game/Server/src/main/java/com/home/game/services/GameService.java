package com.home.game.services;

public interface GameService {
    // метод вызывается для начала игры
    Integer startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname);
   // метод фиксирует выстрел игроков
    void shot(Integer id, String shooterNickname, String targetNickname);
    // метод реализующий конец игры
    void finishGame(Integer gameId, String winnerNickname, String loserNickname);
}
