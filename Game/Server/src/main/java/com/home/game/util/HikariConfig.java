package com.home.game.util;

import com.zaxxer.hikari.HikariDataSource;

public class HikariConfig {

    public HikariDataSource dataSource() {
        com.zaxxer.hikari.HikariConfig hikariConfig = new com.zaxxer.hikari.HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://192.168.183.130:5432/tanks_db");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("myPa$$word");
        hikariConfig.setMaximumPoolSize(10);

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        return dataSource;
    }

}

