package com.home.game.repositories;

import com.home.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;
import java.util.function.Function;

public class PlayersRepositoryImpl implements PlayersRepository {

    private static final String SQL_INSERT =
            "insert into player (ip_address, name, count_wins, count_lose, points_scored) " +
                    "values (?, ?, ?, ?, ?);";

    private static final String SQL_FIND_BY_NAME = "select * from player where name = ?";

    private static final String SQL_SELECT = "select name from player where name = ?";

    private static final String SQL_UPDATE = "update player set " + "ip_address = ?, " + "name = ?, "
            + "count_wins = ?, " + "count_lose = ?, " + "points_scored = ? " + "where id = ?";

    private DataSource dataSource;

    public PlayersRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private final Function<ResultSet, Player> fullInfoPlayer = row -> {
        try {
            return Player.builder()
                    .id(row.getInt("id"))
                    .ipAddress(row.getString("ip_address"))
                    .name(row.getString("name"))
                    .countWins(row.getInt("count_wins"))
                    .countLose(row.getInt("count_lose"))
                    .pointsScored(row.getInt("points_scored"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    private final Function<ResultSet, Player> namePlayer = row -> {
        try {
            return Player.builder()
                    .name(row.getString("name"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };


    public Optional<Player> findByName(String name) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NAME)) {

            statement.setString(1, name);
            try (ResultSet result = statement.executeQuery()) {
                // если из базы ничего не пришло
                if (!result.next()) {
                    return Optional.empty();
                }
                // создаем пользователя
                Player player = fullInfoPlayer.apply(result);
                // вернули результат
                return Optional.of(player);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
    public Player getByNickname(String name) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT)) {

            statement.setString(1, name);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                Player player = namePlayer.apply(result);


                return player;
            } else {
                return null;
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);

        }
    }


    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, player.getIpAddress());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getCountWins());
            statement.setInt(4, player.getCountLose());
            statement.setInt(5, player.getPointsScored());

            int update = statement.executeUpdate();

            if (update != 1) {
                throw new SQLException("Cannot save player");
            }

            ResultSet keys = statement.getGeneratedKeys();
            if (keys.next()) {
                int generatedIdIn = keys.getInt("id");
                player.setId(generatedIdIn);
            } else {
                throw new SQLException("Cannot return id");
            }
            keys.close();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, player.getIpAddress());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getCountWins());
            statement.setInt(4, player.getCountLose());
            statement.setInt(5, player.getPointsScored());
            statement.setInt(6, player.getId());

            int update = statement.executeUpdate();

            if (update != 1) {
                throw new SQLException("Cannot update player");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


}
