package com.home.game.repositories;

import com.home.game.models.Game;
import com.home.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;


public class GamesRepositoryImpl implements GamesRepository {
    // запросы, которые мы посылаем в базу данных
    private static final String SQL_INSERT =
            "insert into game (date, first_player, second_player, fist_player_shots_count," +
            " second_player_shots_count, duration_game) values (?, ?, ?, ?, ?, ?);";

    private static final String SQL_FIND_BY_ID = "select * from game where id = ?";

    private static final String SQL_UPDATE = "update game set " + "date = ?, " + "first_player = ?, " +
            "second_player = ?, " + "fist_player_shots_count = ?, " + "second_player_shots_count = ?, " +
            "duration_game = ? " + "where id = ?";

    //зависимость
    private DataSource dataSource;

    public GamesRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection();
             // RETURN_GENERATED_KEYS означает, что запрос должен вернуть ключи, которые сгенерировала база данных
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setTimestamp(1, game.getGameDate());
            statement.setInt(2, game.getFirstPlayer().getId());
            statement.setInt(3, game.getSecondPlayer().getId());
            statement.setInt(4, game.getFistPlayerShotsCount());
            statement.setInt(5, game.getSecondPlayerShotsCont());
            statement.setLong(6, game.getDurationGame());
            // строк было обновлено
            int update = statement.executeUpdate();

            if (update != 1) {
                throw new SQLException("Cannot save game");
            }
            // получаю ключи, которые сгенерировала база данных
            ResultSet keys = statement.getGeneratedKeys();
            if (keys.next()) {
                // запрашиваю сгенерированный ключ, который называется id
                int generatedId = keys.getInt("id");
                // проставляю
                game.setId(generatedId);
            } else {
                // если база не смогла вернуть сгенерированный ключ
                throw new SQLException("Cannot return id");
            }
            keys.close();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Game findById(Integer id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {

            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                PlayersRepository playersRepository = new PlayersRepositoryImpl(dataSource);

                return Game.builder()
                        .id(result.getInt("id"))
                        .gameDate(result.getTimestamp("date"))
                        .firstPlayer(Player.builder().id(result.getInt("first_player")).build())
                        .secondPlayer(Player.builder().id(result.getInt("second_player")).build())
                        .fistPlayerShotsCount(result.getInt("fist_player_shots_count"))
                        .secondPlayerShotsCont(result.getInt("second_player_shots_count"))
                        .durationGame(result.getLong("duration_game"))
                        .build();


            } else {
                return null;
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setTimestamp(1, game.getGameDate());
            statement.setInt(2, game.getFirstPlayer().getId());
            statement.setInt(3, game.getSecondPlayer().getId());
            statement.setInt(4, game.getFistPlayerShotsCount());
            statement.setInt(5, game.getSecondPlayerShotsCont());
            statement.setLong(6, game.getDurationGame());
            statement.setInt(7, game.getId());

            int update = statement.executeUpdate();

            if (update != 1) {
                throw new SQLException("Cannot update game");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
