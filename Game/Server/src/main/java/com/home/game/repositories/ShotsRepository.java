package com.home.game.repositories;

import com.home.game.models.Shot;

public interface ShotsRepository extends CrudRepository<Shot> {

}
