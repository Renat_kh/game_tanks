package com.home.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor

public class Player {
    private Integer id;
    private String ipAddress;
    private String  name;
    private Integer countWins;
    private Integer countLose;
    private Integer pointsScored;

}
