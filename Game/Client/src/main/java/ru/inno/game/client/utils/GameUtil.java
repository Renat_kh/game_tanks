package ru.inno.game.client.utils;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import javafx.util.Duration;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.sockets.SocketClient;


public class GameUtil {

    private final static int DAMAGE = 5;
    private final static int PLAYER_STEP = 5;

    private AnchorPane pane;
    private Label hpPlayer;
    private Label hpEnemy;
    private SocketClient socketClient;

    private ImageView endGame;
    private Button exit;
    private Label result;

    public GameUtil(MainController controller) {
        this.pane = controller.getPane();
        this.hpPlayer = controller.getHpPlayer();
        this.hpEnemy = controller.getHpEnemy();
        this.endGame = controller.getExitGame();
        this.exit = controller.getExit();
        this.result = controller.getResult();

    }

    public void setSocketClient(SocketClient socketClient) {
        this.socketClient = socketClient;
    }

    // shooterIsEnemy - true - враг стреляет в нас
    public void createBullet(ImageView shooter, ImageView target, boolean shooterIsEnemy) {
        Image image = new Image("/images/bullet.png");
        ImageView bullet = new ImageView(image);
        // ставим пулю, там же, где и находится игрок в данный момент
        bullet.setLayoutX(shooter.getX() + shooter.getLayoutX() + (shooter.getFitWidth() / 2));
        bullet.setLayoutY(shooter.getY() + shooter.getLayoutY());
        // добавляю ее в сцену как вложенный объект относительно панели
        pane.getChildren().add(bullet);


        int value;
        Label hpLabel;
        //выстрел вниз/вверх
        if (shooterIsEnemy) {
            hpLabel = hpPlayer;
            bullet.setRotate(180);
            value = 1;
        } else {
            hpLabel = hpEnemy;
            value = -1;
        }


        // сделали анимацию выстрела
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.005), animation -> {
            bullet.setLayoutY(bullet.getLayoutY() + value);

            if (bullet.getLayoutY() < 0 && bullet.getLayoutY() > 500) {
                pane.getChildren().remove(bullet);
            }

            if (bullet.isVisible() && isIntersects(bullet, target)) {
                bullet.setVisible(false);
                createDamage(hpLabel);

                if (!shooterIsEnemy) {
                    socketClient.sendMessage("damage");
                }
                if (getHpEnemy().getText().equals("0")) {
                    socketClient.sendMessage("STOP");
                }

            }
        }));

        timeline.setCycleCount(453);
        timeline.play();
    }

    private boolean isIntersects(ImageView bullet, ImageView target) {
        return bullet.getBoundsInParent().intersects(target.getBoundsInParent());
    }

    private void createDamage(Label hpLabel) {
        int hpPerson = Integer.parseInt(hpLabel.getText());
         if (hpPerson > 0){
            hpLabel.setText(String.valueOf(hpPerson - DAMAGE));
        }
    }

    public void goLeft(ImageView person) {
        if (person.getX() >= -220) {
            person.setX(person.getX() - PLAYER_STEP);
        }
    }

    public void goRight(ImageView person) {
        if (person.getX() <= 200) {
            person.setX(person.getX() + PLAYER_STEP);
        }
    }
    public Label getHpEnemy() {
        return hpEnemy;
    }

    public Label getHpPlayer() {
        return hpPlayer;
    }

    public void visibleExitButton() {
        endGame.setVisible(true);
        exit.setVisible(true);
        exit.setOnAction(event -> {
            exit.getScene().getRoot().requestFocus();
            socketClient.sendMessage("STOP");
            System.exit(0);
        });

    }

    public void result(String[] parsingString) {
        result.setVisible(true);
        result.setText("Победитель: " + parsingString[0]);


    }
}

