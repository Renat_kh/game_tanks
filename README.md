# Основы разработки ПО на Java, FAQ

 ## Game
 
	Финальным проект в папке Game это клиент-серверная игра “Танки”. В папке находится Client клиент игры 
	с графической визуализации с помощью JavaFX. В папке находится Server серверная часть игры,
	которая обеспечивает связи между двумя игроками с последующем сохранением данных сыгранной игры в СУБД PostgreSQL.
